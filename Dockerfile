FROM adoptopenjdk:11-jre-hotspot
ADD target/betterNBP-0.0.1-SNAPSHOT.jar .
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "betterNBP-0.0.1-SNAPSHOT.jar"]