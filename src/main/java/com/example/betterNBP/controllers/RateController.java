package com.example.betterNBP.controllers;

import com.example.betterNBP.clients.WebClient;
import com.example.betterNBP.exceptions.ConnectionException;
import com.example.betterNBP.exceptions.CurrencyException;
import com.example.betterNBP.models.AnswerDto;
import com.example.betterNBP.models.RateDto;
import com.example.betterNBP.service.LogService;
import com.example.betterNBP.service.RateService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class RateController {

    private final RateService rateService;
    private final LogService logService;
    private final WebClient webClient;


    @CrossOrigin
    @GetMapping("/api")
    public List<String> getCurrency() throws CurrencyException, ConnectionException {
        List<RateDto> rates = webClient.getAllRates();
        logService.saveLogs("get all rates ");
        return rateService.getAllRatesCurrency(rates);
    }

    @CrossOrigin
    @GetMapping("/api/currency")
    public List<RateDto> getAllRates() throws CurrencyException, ConnectionException {
        logService.saveLogs("get all rates currency");
        return rateService.getAllRates();
    }

    @CrossOrigin
    @GetMapping("/api/currency/buy")
    public AnswerDto buyCurrency(@RequestParam("code") String code, @RequestParam("amount") BigDecimal amount) throws CurrencyException, ConnectionException {
        List<RateDto> rates = webClient.getAllRates();
        logService.saveLogs("buy currency"+" "+code+" "+amount);
        return rateService.buyCurrency(code,amount,rates);
    }

    @CrossOrigin
    @GetMapping("/api/currency/sell")
    public AnswerDto sellCurrency(@RequestParam("code") String code, @RequestParam("amount") BigDecimal amount) throws CurrencyException, ConnectionException {
        List<RateDto> rates = webClient.getAllRates();
        logService.saveLogs("sell currency"+" "+code+" "+amount);
        return rateService.sellCurrency(code,amount,rates);
    }
}
