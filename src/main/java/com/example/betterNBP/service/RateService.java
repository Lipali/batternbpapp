package com.example.betterNBP.service;

import com.example.betterNBP.clients.WebClient;
import com.example.betterNBP.exceptions.ConnectionException;
import com.example.betterNBP.exceptions.CurrencyException;
import com.example.betterNBP.models.AnswerDto;
import com.example.betterNBP.models.ExceptionMessage;
import com.example.betterNBP.models.RateDto;
import com.example.betterNBP.models.TechDto;
import com.example.betterNBP.repositories.LogRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class RateService {

    private final WebClient webClient ;
    private final LogService logService;

    public List<RateDto> getAllRates() throws ConnectionException {
        log.info("get all rates");
        logService.saveLogs("OK get all rates");
        return webClient.getAllRates();
    }

    public List<String> getAllRatesCurrency(List<RateDto>rates) throws ConnectionException {
        log.info("get all rates currency");
        logService.saveLogs("OK get all rates currency");

        List<String> result = new LinkedList<>();
        for(RateDto rate : rates){
            String temp =rate.getCurrency();
            result.add(temp);
        }
        return result;
    }
    public AnswerDto buyCurrency(String code , BigDecimal amount,List<RateDto>rates) throws CurrencyException, ConnectionException {
        log.info("buy currency"+" "+code+" "+amount);
        logService.saveLogs("OK : buy currency"+" "+code+" "+amount);

        BigDecimal result = null;
        if ((codeValidator(code,rates)==true)&&(amountValidator(amount)==true)) {
            for (RateDto temp : rates) {
                if (temp.getCode().equals(code)) {
                    result = amount.divide(temp.getAsk(), 2, RoundingMode.HALF_DOWN);
                }
            }
        }
        else{
            String message = ExceptionMessage.BAD_PARAMETERS.getMessage();
            logService.saveLogs("ERROR : " +message+" " + code+" " +amount);
            throw new CurrencyException("invalid parameters");}
        return new AnswerDto(code ,result);
    }
    public AnswerDto sellCurrency(String code , BigDecimal amount,List<RateDto>rates) throws CurrencyException, ConnectionException {
        log.info("sell currency"+" "+code+" "+amount);
        logService.saveLogs("OK : sell currency"+" "+code+" "+amount);

        BigDecimal result = null;
        if ((codeValidator(code,rates)==true)&&(amountValidator(amount)==true)) {
            for (RateDto temp : rates) {
                if (temp.getCode().equals(code)) {
                    result = temp.getBid().multiply(amount);
                }
            }
            result = result.setScale(2, RoundingMode.HALF_DOWN);
        }
        else{
            String message = ExceptionMessage.BAD_PARAMETERS.getMessage();
            logService.saveLogs("ERROR : " +message+" " + code+" " +amount);
            throw new CurrencyException("invalid parameters");
        }
        return new AnswerDto("PLN" ,result);
    }

    public Boolean codeValidator(String code,List<RateDto>rates){
        boolean flag = false;
        for (RateDto temp :rates){
            if (temp.getCode().equals(code)){
                flag =true;
                break;
            }
        }
        return flag;
    }
    public boolean amountValidator(BigDecimal amount){
        if(amount.doubleValue()>=0.01)
            return true;
        else
            return false;
    }

}
