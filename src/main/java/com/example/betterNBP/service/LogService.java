package com.example.betterNBP.service;

import com.example.betterNBP.models.Log;
import com.example.betterNBP.repositories.LogRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
@Slf4j
public class LogService {

    private final LogRepository logRepository ;

    public void saveLogs(String message){
        Log log = Log.builder()
                .message(message)
                .time(LocalDateTime.now())
                .build();
        logRepository.save(log);
    }
}
