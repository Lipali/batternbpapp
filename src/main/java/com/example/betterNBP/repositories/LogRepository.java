package com.example.betterNBP.repositories;


import com.example.betterNBP.models.Log;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LogRepository extends JpaRepository<Log, Long> {

}
