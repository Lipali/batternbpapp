package com.example.betterNBP.exceptions;

public class CurrencyException extends Exception{
    public CurrencyException() {}
    public CurrencyException(String s){
        super(s);
    }
}
