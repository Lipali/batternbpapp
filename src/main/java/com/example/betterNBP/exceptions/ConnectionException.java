package com.example.betterNBP.exceptions;

public class ConnectionException extends Exception{
    public ConnectionException() {}
    public ConnectionException(String s){
        super(s);
    }
}
