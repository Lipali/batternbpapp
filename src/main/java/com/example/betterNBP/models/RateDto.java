package com.example.betterNBP.models;

import lombok.*;

import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class RateDto {
    private String currency;
    private String code;
    private BigDecimal bid;
    private BigDecimal ask;
}
