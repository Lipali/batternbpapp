package com.example.betterNBP.models;


import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum ExceptionMessage {
    BAD_PARAMETERS("Bad parameters"),
    NBP_TABLE_NOT_FOUND("NbpTable not found");

    private final String message;

    public String getMessage(String key) {
        return this.message + key;
    }

    public String getMessage() {
        return this.message;
    }
}
