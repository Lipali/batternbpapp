package com.example.betterNBP.clients;

import com.example.betterNBP.exceptions.ConnectionException;
import com.example.betterNBP.models.ExceptionMessage;
import com.example.betterNBP.models.RateDto;
import com.example.betterNBP.models.TechDto;
import com.example.betterNBP.service.LogService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.annotations.common.util.impl.Log;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Slf4j
@Component
@RequiredArgsConstructor
public class WebClient {

    private final RestTemplate restTemplate= new RestTemplate();
    private final static String CURRENCY_EXCHANGE_RATES_TABLE = "http://api.nbp.pl/api/exchangerates/tables/c?format=json";
    private final LogService logService;

    public List<RateDto> getAllRates() throws ConnectionException {
        log.info("get all rates -api connection");
        logService.saveLogs("get all rates -api connection");
        TechDto[] techDtos = restTemplate.getForObject(CURRENCY_EXCHANGE_RATES_TABLE, TechDto[].class);
        if (techDtos==null){
            String message = ExceptionMessage.NBP_TABLE_NOT_FOUND.getMessage();
            logService.saveLogs("ERROR "+message);
            throw new ConnectionException(message);
        }
        return techDtos[0].getRates();
    }

}
