package com.example.betterNBP;

import com.example.betterNBP.exceptions.ConnectionException;
import com.example.betterNBP.exceptions.CurrencyException;
import com.example.betterNBP.models.AnswerDto;
import com.example.betterNBP.models.RateDto;
import com.example.betterNBP.service.LogService;
import com.example.betterNBP.service.RateService;

import org.assertj.core.internal.bytebuddy.utility.dispatcher.JavaDispatcher;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.FluentQuery;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.mock;

@DataJpaTest
public class RateServiceTest {

    @Mock
    LogService logService;
    @InjectMocks
    RateService rateService ;

    RateDto rateA = RateDto.builder()
            .currency("dolar amerykański")
            .code("USD")
            .bid(BigDecimal.valueOf(4.4445))
            .ask(BigDecimal.valueOf(4.5343))
            .build();
    RateDto rateB = RateDto.builder()
            .currency("euro")
            .code("EUR")
            .bid(BigDecimal.valueOf(4.6391))
            .ask(BigDecimal.valueOf(4.7329))
            .build();
    List<RateDto> list = List.of(rateA,rateB);


    @Test
    public void codeValidator_shouldReturnTrueWhenUSD() {

        //given:


        boolean exeptedValue = true;
        //when
        Boolean  result = rateService.codeValidator("USD",list);

        //then
        Assertions.assertEquals(exeptedValue, result);
    }
    @Test
    public void codeValidator_shouldReturnFalseWhenRRR() {

        //given:


        boolean exeptedValue = false;
        //when
        Boolean  result = rateService.codeValidator("RRR",list);

        //then
        Assertions.assertEquals(exeptedValue, result);
    }
    @Test
    public void amountValidator_shouldReturnFalseWhenMinus() {
        //given:
        BigDecimal testValue = BigDecimal.valueOf(-3);
        boolean exeptedValue = false;

        //when

        Boolean  result = rateService.amountValidator(testValue);

        //then
        Assertions.assertEquals(exeptedValue, result);

    }
    @Test
    public void amountValidator_shouldReturnTrueWhenGoodValue() {
        //given:
        BigDecimal testValue = BigDecimal.valueOf(3.44);
        boolean exeptedValue = true;

        //when
        Boolean  result = rateService.amountValidator(testValue);

        //then
        Assertions.assertEquals(exeptedValue, result);

    }
    @Test
    public void buyCurrency_shouldReturnTrueWhenGoodValue() throws CurrencyException, ConnectionException {
        //given:
        AnswerDto exeptedValue = new AnswerDto("USD",BigDecimal.valueOf(11.03));

        //when
        AnswerDto  result = rateService.buyCurrency("USD", BigDecimal.valueOf(50),list);

        //then
        Assertions.assertEquals(exeptedValue.getCode(), result.getCode());
        Assertions.assertEquals(exeptedValue.getValue(), result.getValue());

    }
    @Test
    public void sellCurrency_shouldReturnTrueWhenGoodValue() throws CurrencyException, ConnectionException {
        //given:
        AnswerDto exeptedValue = new AnswerDto("PLN",BigDecimal.valueOf(222.22));

        //when
        AnswerDto  result = rateService.sellCurrency("USD", BigDecimal.valueOf(50),list);

        //then
        Assertions.assertEquals(exeptedValue.getCode(), result.getCode());
        Assertions.assertEquals(exeptedValue.getValue(), result.getValue());

    }





}
